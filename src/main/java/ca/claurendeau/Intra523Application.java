package ca.claurendeau;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Intra523Application {
    
	public static void main(String[] args) {
		SpringApplication.run(Intra523Application.class, args);
	}
	
	/*@Bean
    public CommandLineRunner demo(VOTRE_SERVICE service) {
        return (args) -> {
            service.createInitialPersonnes();
        };
    }*/
}

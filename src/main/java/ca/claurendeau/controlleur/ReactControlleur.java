package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.multitiers.repository.UserRepository;

import ca.claurendeau.domaine.Message;

@Autowired
private User user;

@RestController
public class ReactControlleur {

	@CrossOrigin
	@GetMapping("VOTRE_ENDPOINT_DE_MESSAGE_ICI")
	public void fonctionA() {

	}

	@CrossOrigin
	@PostMapping(value="/getValue")
	public @ResponseBody String fonctionB(@RequestBody String nom) {
		System.out.println("Sending");
		return new Gson().toJson("{'nom':'Alain','email':'testing@test.com','id':'10'}");
	}
	 
}
